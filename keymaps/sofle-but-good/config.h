#pragma once

/* The way how "handedness" is decided (which half is which),
see https://docs.qmk.fm/#/feature_split_keyboard?id=setting-handedness
for more options.
*/

// #define MK_KINETIC_SPEED
// #define MOUSEKEY_INITIAL_SPEED 200
// #define MOUSEKEY_ACCELERATED_SPEED 2000

#define MOUSEKEY_INERTIA
#define MOUSEKEY_MOVE_DELTA 5

#define BOTH_SHIFTS_TURNS_ON_CAPS_WORD
#define ONESHOT_TIMEOUT 1000

#undef  TAPPING_TERM
#define TAPPING_TERM 200
#define TAPPING_TERM_PER_KEY

#define EE_HANDS
#define SPLIT_USB_TIMEOUT 1000
#define SPLIT_WATCHDOG_ENABLE
#define SPLIT_WATCHDOG_TIMEOUT 3000

#define UNICODE_SELECTED_MODES UNICODE_MODE_LINUX

// #define PERMISSIVE_HOLD
