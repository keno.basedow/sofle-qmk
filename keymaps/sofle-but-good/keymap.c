#include QMK_KEYBOARD_H
#include "tapdance.h"

//Tap Dance Declarations
enum {
    TD_CRLY_SQR,
    TD_CRLY_SQRR,
    TD_MINUS,
    TH_MINUS,
    TH_LCRLY_SQR,
};

enum sofle_layers {
    /* _M_XYZ = Mac Os, _W_XYZ = Win/Linux */
    _QWERTY,
    _COLEMAK,
    _GAMING,
    _LOWER,
    _RAISE,
    _ADJUST,
    _LAYER3,
};

enum custom_keycodes {
    KC_QWERTY = SAFE_RANGE,
    KC_COLEMAK,
    KC_PRVWD,
    KC_NXTWD,
    KC_LSTRT,
    KC_LEND,
    KC_DLINE,
    KC_10UP,
    KC_10DN,
    KC_TDMINUS
};

enum unicode_names {
    POOP,
};

const uint32_t unicode_map[] PROGMEM = {
    [POOP] = 0x1F4A9,
};

tap_dance_action_t tap_dance_actions[] = {
    [TD_CRLY_SQR]  = ACTION_TAP_DANCE_DOUBLE(KC_LCBR, KC_LBRC),
    [TD_CRLY_SQRR]  = ACTION_TAP_DANCE_DOUBLE(KC_RCBR, KC_RBRC),
    [TD_MINUS] = ACTION_TAP_DANCE_DOUBLE(KC_MINUS, KC_UNDERSCORE),
    [TH_MINUS] = ACTION_TAP_DANCE_TAP_HOLD(KC_MINUS, KC_UNDERSCORE),
    [TH_LCRLY_SQR] = ACTION_TAP_DANCE_TAP_HOLD(KC_LEFT_CURLY_BRACE, KC_LBRC)
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    /*
    * QWERTY
    * ,-----------------------------------------.                    ,-----------------------------------------.
    * | F12  |  F1  |  F2  |  F3  |  F4  |  F5  |                    |  F6  |  F7  |  F8  |  F9  | F10  | F11  |
    * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
    * | Tab  |   Q  |   W  |   E  |   R  |   T  |                    |   Y  |   U  |   I  |   O  |   P  | Bspc |
    * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
    * | Tab  |   A  |   S  |   D  |   F  |   G  |-------.    ,-------|   H  |   J  |   K  |   L  |   ;  |  '   |
    * |------+------+------+------+------+------|  MUTE |    |       |------+------+------+------+------+------|
    * |LShift|   Z  |   X  |   C  |   V  |   B  |-------|    |-------|   N  |   M  |   ,  |   .  |   /  |RShift|
    * `-----------------------------------------/       /     \      \-----------------------------------------'
    *            | LAlt | LGUI | LCTR |LOWER | /Enter  /       \Space \  |RAISE |AltGr | RAlt | RCtl |
    *            |      |      |      |      |/       /         \      \ |      |      |      |      |
    *            `-----------------------------------'           '------''---------------------------'
    */

    [_QWERTY] = LAYOUT(
    KC_F12,        KC_F1 , KC_F2 ,  KC_F3 ,  KC_F4 ,  KC_F5,                    KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10 ,  KC_F11,
    KC_TAB,        KC_Q,   KC_W,    KC_E,    KC_R,    KC_T,                     KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,     KC_BSPC,
    KC_TAB,        KC_A,   KC_S,    KC_D,    KC_F,    KC_G,                     KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN,  KC_QUOT,
    KC_LSFT,       KC_Z,   KC_X,    KC_C,    KC_V,    KC_B, KC_MUTE,    XXXXXXX,KC_N,    KC_M, KC_COMM,    KC_DOT,  KC_SLSH,  KC_RSFT,
           KC_LALT,KC_LGUI,KC_LCTL, MO(_LOWER), LT(_LAYER3, KC_ENT),    KC_SPC,  MO(_RAISE), KC_ALGR, KC_RALT, KC_RCTL
    ),
    /*
    * COLEMAK
    * ,-----------------------------------------.                    ,-----------------------------------------.
    * | F12  |  F1  |  F2  |  F3  |  F4  |  F5  |                    |  F6  |  F7  |  F8  |  F9  | F10  | F11  |
    * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
    * | Tab  |   Q  |   W  |   F  |   P  |   G  |                    |   J  |   L  |   U  |   Y  |   ;  | Bspc |
    * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
    * | Tab  |   A  |   R  |   S  |   T  |   D  |-------.    ,-------|   H  |   N  |   E  |   I  |   O  |  '   |
    * |------+------+------+------+------+------|  MUTE |    |       |------+------+------+------+------+------|
    * |LShift|   Z  |   X  |   C  |   V  |   B  |-------|    |-------|   K  |   M  |   ,  |   .  |   /  |RShift|
    * `-----------------------------------------/       /     \      \-----------------------------------------'
    *            | LGUI | LAlt | LCTR |LOWER | /Enter  /       \Space \  |RAISE |AltGr | RAlt | RCtl |
    *            |      |      |      |      |/       /         \      \ |      |      |      |      |
    *            `----------------------------------'           '------''---------------------------'
    */

    [_COLEMAK] = LAYOUT(
    KC_F12,         KC_F1 , KC_F2 ,  KC_F3 ,  KC_F4 ,  KC_F5,                     KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10 ,  KC_F11,
    KC_TAB,         KC_Q,   KC_W,    KC_F,    KC_P,    KC_G,                      KC_J,    KC_L,    KC_U,    KC_Y,    KC_SCLN,  KC_BSPC,
    KC_TAB,         KC_A,   KC_R,    KC_S,    KC_T,    KC_D,                      KC_H,    KC_N,    KC_E,    KC_I,    KC_O,     KC_QUOT,
    KC_LSFT,        KC_Z,   KC_X,    KC_C,    KC_V,    KC_B, KC_MUTE,     XXXXXXX,KC_K,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH,  KC_RSFT,
            KC_LALT,KC_LGUI,KC_LCTL, MO(_LOWER), LT(_LAYER3, KC_ENT),     KC_SPC,  MO(_RAISE), KC_ALGR, KC_RALT, KC_RCTL
    ),
    [_GAMING] = LAYOUT(
    KC_TAB  , _______, _______, _______, _______, _______,                       _______, _______, _______, _______, _______, _______,
    _______ , _______, _______, _______, _______, _______,                       _______, _______, _______, _______, _______, _______,
    KC_LSFT , _______, _______, _______, _______, _______,                       _______, _______, _______, _______, _______, _______,
    KC_LCTL , _______, _______, _______, _______, _______, _______,     _______, _______, _______, _______, _______, _______, _______,
                       _______, _______, _______, _______, _______,     _______, _______, _______, _______, _______
    ),
    /* LOWER
    * ,-----------------------------------------.                    ,-----------------------------------------.
    * | F12  |  F1  |  F2  |  F3  |  F4  |  F5  |                    |  F6  |  F7  |  F8  |  F9  | F10  | F11  |
    * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
    * |  `   |   1  |   2  |   3  |   4  |   5  |                    |   6  |   7  |   8  |   9  |   0  | Bspc |
    * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
    * |  ^   |   _  |   -  |   +  |   =  |   %  |-------.    ,-------|   !  |   &  |   *  |   (  |   )  |   |  |
    * |------+------+------+------+------+------|  MUTE |    |       |------+------+------+------+------+------|
    * | Shift|  $   |  @   |  #   |   {  |   [  |-------|    |-------|   ]  |   }  |   <  |   >  |   \  | Shift|
    * `-----------------------------------------/       /     \      \-----------------------------------------'
    *            | LAlt | LGUI | LCTR |LOWER | /Enter  /       \Space \  |RAISE |AltGr | RAlt | RCtl |
    *            |      |      |      |      |/       /         \      \ |      |      |      |      |
    *            `-----------------------------------'           '------''---------------------------'
    */
    [_LOWER] = LAYOUT(
    KC_F12,   KC_F1 ,  KC_F2 ,  KC_F3 ,  KC_F4 ,  KC_F5,                       KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10 , KC_F11,
    KC_GRV,   KC_1,    KC_2,    KC_3,    KC_4,    KC_5,                        KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_BSPC,
    KC_CIRC,  KC_UNDS, KC_MINUS,KC_PLUS, KC_EQL,  KC_PERC,                     KC_EXLM, KC_AMPR, KC_ASTR, KC_LPRN, KC_RPRN, KC_PIPE,
    _______,  KC_DLR,  KC_AT,   KC_HASH, KC_LCBR, KC_LBRC, _______,   _______, KC_RBRC, KC_RCBR, KC_LT,   KC_GT,   KC_BSLS, _______,
                        _______, _______, _______, _______, KC_ENT,   KC_SPC, _______, _______, _______, _______
    ),
    /* RAISE
    * ,----------------------------------------.                     ,-----------------------------------------.
    * | F12  |  F1  |  F2  |  F3  |  F4  |  F5  |                    |  F6  |  F7  |  F8  |  F9  | F10  | F11  |
    * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
    * | Esc  | Ins  | Pscr | Menu |      |      |                    | PgUp | PWrd |  Up  | NWrd | DLine| Bspc |
    * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
    * | Esc  | LAt  | LCtl |LShift|      |      |-------.    ,-------| PgDn | Left | Down |Right | Del  |DWordB|
    * |------+------+------+------+------+------|  MUTE |    |       |------+------+------+------+------+------|
    * |Shift | Undo |  Cut | Copy | Paste| Redo |-------|    |-------|      | LStr |      | LEnd |      | Shift|
    * `-----------------------------------------/       /     \      \-----------------------------------------'
    *            | LAlt | LGUI | LCTR |LOWER | /Space  /       \Enter \  |RAISE |AltGr | RAlt | RCtl |
    *            |      |      |      |      |/       /         \      \ |      |      |      |      |
    *            `-----------------------------------'           '------''---------------------------'
    */
    [_RAISE] = LAYOUT(
    KC_F12,   KC_F1 ,   KC_F2 ,   KC_F3 ,   KC_F4 ,   KC_F5,                          KC_F6,   KC_F7,      KC_F8,   KC_F9 ,   KC_F10 , KC_F11,
    KC_ESC,   KC_INS,   KC_PSCR,  KC_APP,   _______,  _______,                        KC_PGUP, KC_PRVWD,   KC_UP,   KC_NXTWD, LCTL(KC_BSPC), KC_BSPC,
    KC_ESC,   KC_LALT,  KC_LCTL,  KC_LSFT,  _______,  _______,                        KC_PGDN, KC_LEFT,    KC_DOWN, KC_RGHT,  KC_DEL,  LCTL(KC_DEL),
    _______,  KC_UNDO,  KC_CUT,   KC_COPY,  KC_PASTE,LCTL(KC_Y),KC_MPLY,    _______,  XXXXXXX, KC_LSTRT,   XXXXXXX, KC_LEND,  XXXXXXX, _______,
                            _______, _______, _______, _______, _______,    _______, _______, _______, _______, _______
    ),
    /* ADJUST
    * ,-----------------------------------------.                    ,-----------------------------------------.
    * |      |      |      |      |      |      |                    |      |      |      |      |      |      |
    * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
    * |QK_BOOT|     |QWERTY|COLEMAK|     |      |                    |      |      | 10UP |      |      |      |
    * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
    * |      |      |MACWIN|      |      |      |-------.    ,-------|      |      |10DOWN|      |      |      |
    * |------+------+------+------+------+------|  MUTE |    |       |------+------+------+------+------+------|
    * |      |      |      |      |      |      |-------|    |-------|      | PREV | PLAY | NEXT |      |      |
    * `-----------------------------------------/       /     \      \-----------------------------------------'
    *            | LAlt | LGUI | LCTR |LOWER | /Enter  /       \Space \  |RAISE |AltGr | RAlt | RCtl |
    *            |      |      |      |      |/       /         \      \ |      |      |      |      |
    *            `----------------------------------'           '------''---------------------------'
    */
    [_ADJUST] = LAYOUT(
    XXXXXXX , XXXXXXX, XXXXXXX ,   XXXXXXX,    XXXXXXX,     XXXXXXX,                       XXXXXXX, XXXXXXX,    XXXXXXX,    XXXXXXX,     XXXXXXX, XXXXXXX,
    QK_BOOT , XXXXXXX, DF(_QWERTY),XXXXXXX,DF(_GAMING),     XXXXXXX,                       XXXXXXX, XXXXXXX,    KC_10UP,    XXXXXXX,     XXXXXXX, XXXXXXX,
    XXXXXXX , XXXXXXX, NK_TOGG,    XXXXXXX,    XXXXXXX,     XXXXXXX,                       XXXXXXX, XXXXXXX,    KC_10DN,    XXXXXXX,     XXXXXXX, XXXXXXX,
    XXXXXXX , XXXXXXX, XXXXXXX,    XXXXXXX,    XXXXXXX,     XXXXXXX, XXXXXXX,     XXXXXXX, X(POOP), KC_MPRV,    KC_MPLY,    KC_MNXT,     XXXXXXX, XXXXXXX,
                                 _______, _______, _______, _______, _______,     _______, _______, _______, _______, _______
    ),
    [_LAYER3] = LAYOUT(
    XXXXXXX , XXXXXXX, XXXXXXX,    XXXXXXX ,   XXXXXXX,     XXXXXXX,                       XXXXXXX, XXXXXXX,    XXXXXXX,    XXXXXXX,     XXXXXXX, XXXXXXX,
    XXXXXXX , XXXXXXX, XXXXXXX,    XXXXXXX,    XXXXXXX,     XXXXXXX,                       XXXXXXX, XXXXXXX,    KC_MS_UP,   XXXXXXX,     XXXXXXX, XXXXXXX,
    XXXXXXX , XXXXXXX, XXXXXXX,    KC_MS_BTN2, KC_MS_BTN1,  KC_VOLU,                       KC_WH_U, KC_MS_LEFT, KC_MS_DOWN, KC_MS_RIGHT, XXXXXXX, XXXXXXX,
    XXXXXXX , XXXXXXX, XXXXXXX,    XXXXXXX,    XXXXXXX,     KC_VOLD, XXXXXXX,     XXXXXXX, KC_WH_D, XXXXXXX,    XXXXXXX,    XXXXXXX,     XXXXXXX, XXXXXXX,
                                 _______, _______, _______, _______, _______,     _______, _______, _______, _______, _______
    ),
};

#ifdef OLED_ENABLE

static void render_logo(void) {
    static const char PROGMEM qmk_logo[] = {
        0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8a,0x8b,0x8c,0x8d,0x8e,0x8f,0x90,0x91,0x92,0x93,0x94,
        0xa0,0xa1,0xa2,0xa3,0xa4,0xa5,0xa6,0xa7,0xa8,0xa9,0xaa,0xab,0xac,0xad,0xae,0xaf,0xb0,0xb1,0xb2,0xb3,0xb4,
        0xc0,0xc1,0xc2,0xc3,0xc4,0xc5,0xc6,0xc7,0xc8,0xc9,0xca,0xcb,0xcc,0xcd,0xce,0xcf,0xd0,0xd1,0xd2,0xd3,0xd4,0
    };

    oled_write_P(qmk_logo, false);
}

static void print_status_narrow(void) {
    // Print current mode
    oled_write_P(PSTR("\n\n"), false);
    oled_write_ln_P(PSTR("MODE"), false);
    oled_write_ln_P(PSTR(""), false);
    if (keymap_config.swap_lctl_lgui) {
        oled_write_ln_P(PSTR("MAC"), false);
    } else {
        oled_write_ln_P(PSTR("WIN"), false);
    }

    switch (get_highest_layer(default_layer_state)) {
        case _QWERTY:
            oled_write_ln_P(PSTR("Qwrt"), false);
            break;
        case _COLEMAK:
            oled_write_ln_P(PSTR("Clmk"), false);
            break;
        default:
            oled_write_P(PSTR("Undef"), false);
    }
    oled_write_P(PSTR("\n\n"), false);
    // Print current layer
    oled_write_ln_P(PSTR("LAYER"), false);
    switch (get_highest_layer(layer_state)) {
        case _COLEMAK:
        case _QWERTY:
            oled_write_P(PSTR("Base\n"), false);
            break;
        case _RAISE:
            oled_write_P(PSTR("Raise"), false);
            break;
        case _LOWER:
            oled_write_P(PSTR("Lower"), false);
            break;
        case _ADJUST:
            oled_write_P(PSTR("Adj\n"), false);
            break;
        default:
            oled_write_ln_P(PSTR("Undef"), false);
    }
    oled_write_P(PSTR("\n\n"), false);
    led_t led_usb_state = host_keyboard_led_state();
    oled_write_ln_P(PSTR("CPSLK"), led_usb_state.caps_lock);
}

oled_rotation_t oled_init_user(oled_rotation_t rotation) {
    if (is_keyboard_master()) {
        return OLED_ROTATION_270;
    }
    return rotation;
}

bool oled_task_user(void) {
    if (is_keyboard_master()) {
        print_status_narrow();
    } else {
        render_logo();
    }
    return false;
}

#endif

layer_state_t layer_state_set_user(layer_state_t state) {
    return update_tri_layer_state(state, _LOWER, _RAISE, _ADJUST);
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        case KC_QWERTY:
            if (record->event.pressed) {
                set_single_persistent_default_layer(_QWERTY);
            }
            return false;
        case KC_COLEMAK:
            if (record->event.pressed) {
                set_single_persistent_default_layer(_COLEMAK);
            }
            return false;
        case KC_PRVWD:
            if (record->event.pressed) {
                if (keymap_config.swap_lctl_lgui) {
                    register_mods(mod_config(MOD_LALT));
                    register_code(KC_LEFT);
                } else {
                    register_mods(mod_config(MOD_LCTL));
                    register_code(KC_LEFT);
                }
            } else {
                if (keymap_config.swap_lctl_lgui) {
                    unregister_mods(mod_config(MOD_LALT));
                    unregister_code(KC_LEFT);
                } else {
                    unregister_mods(mod_config(MOD_LCTL));
                    unregister_code(KC_LEFT);
                }
            }
            break;
        case KC_NXTWD:
             if (record->event.pressed) {
                if (keymap_config.swap_lctl_lgui) {
                    register_mods(mod_config(MOD_LALT));
                    register_code(KC_RIGHT);
                } else {
                    register_mods(mod_config(MOD_LCTL));
                    register_code(KC_RIGHT);
                }
            } else {
                if (keymap_config.swap_lctl_lgui) {
                    unregister_mods(mod_config(MOD_LALT));
                    unregister_code(KC_RIGHT);
                } else {
                    unregister_mods(mod_config(MOD_LCTL));
                    unregister_code(KC_RIGHT);
                }
            }
            break;
        case KC_LSTRT:
            if (record->event.pressed) {
                if (keymap_config.swap_lctl_lgui) {
                     //CMD-arrow on Mac, but we have CTL and GUI swapped
                    register_mods(mod_config(MOD_LCTL));
                    register_code(KC_LEFT);
                } else {
                    register_code(KC_HOME);
                }
            } else {
                if (keymap_config.swap_lctl_lgui) {
                    unregister_mods(mod_config(MOD_LCTL));
                    unregister_code(KC_LEFT);
                } else {
                    unregister_code(KC_HOME);
                }
            }
            break;
        case KC_LEND:
            if (record->event.pressed) {
                if (keymap_config.swap_lctl_lgui) {
                    //CMD-arrow on Mac, but we have CTL and GUI swapped
                    register_mods(mod_config(MOD_LCTL));
                    register_code(KC_RIGHT);
                } else {
                    register_code(KC_END);
                }
            } else {
                if (keymap_config.swap_lctl_lgui) {
                    unregister_mods(mod_config(MOD_LCTL));
                    unregister_code(KC_RIGHT);
                } else {
                    unregister_code(KC_END);
                }
            }
            break;
        case KC_COPY:
            if (record->event.pressed) {
                register_mods(mod_config(MOD_LCTL));
                register_code(KC_C);
            } else {
                unregister_mods(mod_config(MOD_LCTL));
                unregister_code(KC_C);
            }
            return false;
        case KC_PASTE:
            if (record->event.pressed) {
                register_mods(mod_config(MOD_LCTL));
                register_code(KC_V);
            } else {
                unregister_mods(mod_config(MOD_LCTL));
                unregister_code(KC_V);
            }
            return false;
        case KC_CUT:
            if (record->event.pressed) {
                register_mods(mod_config(MOD_LCTL));
                register_code(KC_X);
            } else {
                unregister_mods(mod_config(MOD_LCTL));
                unregister_code(KC_X);
            }
            return false;
            break;
        case KC_UNDO:
            if (record->event.pressed) {
                register_mods(mod_config(MOD_LCTL));
                register_code(KC_Z);
            } else {
                unregister_mods(mod_config(MOD_LCTL));
                unregister_code(KC_Z);
            }
            return false;
        case TD(TH_MINUS):
        case TD(TH_LCRLY_SQR):
            ;
            tap_dance_action_t *action = &tap_dance_actions[TD_INDEX(keycode)];

            if (!record->event.pressed && action->state.count && !action->state.finished) {
                tap_dance_tap_hold_t *tap_hold = (tap_dance_tap_hold_t*) action->user_data;
                tap_code16(tap_hold->tap);
            }

            return true;
        case KC_10UP:
            if (record->event.pressed) {
                for (short i = 0; i < 10; i++)
                    register_code(KC_UP);
            } else {
                unregister_code(KC_UP);
            }
            return false;
        case KC_10DN:
            if (record->event.pressed) {
                for (short i = 0; i < 10; i++)
                    register_code(KC_DOWN);
            } else {
                unregister_code(KC_DOWN);
            }
            return false;
    }

    return true;
}

void tap_dance_tap_hold_finished(tap_dance_state_t *state, void *user_data) {
    tap_dance_tap_hold_t *tap_hold = (tap_dance_tap_hold_t*) user_data;

    if (state->pressed) {
        if (state->count == 1
#ifndef PERMISSIVE_HOLD
            && !state->interrupted
#endif
        ) {
            register_code16(tap_hold->hold);
            tap_hold->held = tap_hold->hold;
        } else {
            register_code16(tap_hold->tap);
            tap_hold->held = tap_hold->tap;
        }
    }
}

void tap_dance_tap_hold_reset(tap_dance_state_t *state, void *user_data) {
    tap_dance_tap_hold_t *tap_hold = (tap_dance_tap_hold_t*) user_data;

    if (tap_hold->held) {
        unregister_code16(tap_hold->held);
        tap_hold->held = 0;
    }
}

bool caps_word_press_user(uint16_t keycode) {
    // return true -> continue caps word
    // return false -> end caps word and send this key code un-shifted
    switch (keycode) {
        // Keycodes that continue Caps Word, with shift applied.
        case KC_A ... KC_Z:
            add_weak_mods(MOD_BIT(KC_LSFT));  // Apply shift to next key.
            return true;

        // Keycodes that continue Caps Word, without shifting.
        case KC_1 ... KC_0:
        case KC_BSPC:
        case KC_DEL:
        case KC_UNDS:
        case TD(TD_MINUS): // Custom tap dance handling
        case TD(TH_MINUS):
            return true;

        default:
            return false;  // Deactivate Caps Word.
    }
}

uint16_t get_tapping_term(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        case OSM(MOD_LSFT):
        case OSM(MOD_RSFT):
            return 150;
        case TD(TH_MINUS):
            return 100;
        default:
            return TAPPING_TERM;
    }
}

#ifdef ENCODER_ENABLE
bool encoder_update_user(uint8_t index, bool clockwise) {
    if (index == 0) {
        if (layer_state_is(_RAISE)) {
            tap_code(clockwise ? KC_RIGHT : KC_LEFT);
        } else {
            tap_code(clockwise ? KC_VOLU : KC_VOLD);
        }
    } else if (index == 1) {
        if (layer_state_is(_LOWER)) {
            tap_code(clockwise ? KC_PGDN : KC_PGUP);
        } else {
			if (clockwise) {
				tap_code(KC_TAB);
			} else {
				register_code(KC_LSFT);
				tap_code(KC_TAB);
				unregister_code(KC_LSFT);
			}
        }
    }

    return false;
}
#endif
