# sofle-qmk



## My files for my sofle keyboard



### Keymap

Copy content of keymaps folder to
qmk_firmware\keyboards\sofle

### FreeCAD

freecad folder contains FreeCAD file from Miko.
It contains base for left and right and a lid hold be three magnets.

### Layout

layout folder contains json file for qmk_configurator and pictures of layout for learning.

### Flash

Flash with these commands:

Right:

qmk flash -kb sofle/rev1 -km sofle-but-good -bl uf2-split-right

Left:

qmk flash -kb sofle/rev1 -km sofle-but-good -bl uf2-split-left
